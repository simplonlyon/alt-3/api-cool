
import * as authentication from '@feathersjs/authentication';
import { Forbidden, NotFound } from '@feathersjs/errors';
import { HookContext } from '@feathersjs/feathers';
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [authenticate('jwt'), assignUser()],
    update: [authenticate('jwt'), ownerOnly()],
    patch: [authenticate('jwt'), ownerOnly()],
    remove: [authenticate('jwt'), ownerOnly()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
function assignUser() {
  return (context: HookContext) => {
    if (context.params.user) {
      context.data.userId = context.params.user.id;
    }
    return context;
  };
}

function ownerOnly() {
  return async (context:HookContext) => {
    if (context.params.user) {
      const offerService = context.app.service('offer');
      const offer = await offerService.get(context.id);
      if(!offer) {
        throw new NotFound();
      }
      if(offer.userId != context.params.user.id) {
      
        throw new Forbidden('Only owner can modify');
      }

      

    }
    return context;
  }
}