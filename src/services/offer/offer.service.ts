// Initializes the `offer` service on path `/offer`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Offer } from './offer.class';
import createModel from '../../models/offer.model';
import hooks from './offer.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'offer': Offer & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/offer', new Offer(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('offer');

  service.hooks(hooks);
}
